import java.util.Scanner;
public class DecToOthers{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        boolean loop = true;
        while(loop){
            try{
                System.out.print("Masukkan num  = ");
                int num = input.nextInt();
                System.out.print("Masukkan base = ");
                int base = input.nextInt();
                printBase(num,base);
                loop = false;
            }catch(Exception e){
                System.out.println("Masukkan angka");
                input.next();
            }
        }
    }

    /*static void printBase(int num, int base){
        int rem = 1;
        String digits = "0123456789abcdef";
        String result = "";

        Langkah Iterasi
        while(num!=0){
            rem=num%base;
            num=num/base;
            result = result.concat(digits.charAt(rem)+"");
        }

        Mencetak reverse dari result
        for(int i=result.length()-1;i>=0;i--){
            System.out.print(result.charAt(i));
        }
    }*/

    static void printBase(int num, int base){
        int rem = 1;
        String digits = "0123456789abcdef";
        String result = "";
        if(num>=base){
            printBase(num/base,base);
        }
        rem=num%base;
        result = result.concat(digits.charAt(rem)+"");
        System.out.print(result+"");
    }
}