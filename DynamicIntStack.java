class DynamicIntStack{
    private IntStackNode top;
    class IntStackNode{
        int data;
        IntStackNode next;

        IntStackNode(int n){
            data = n;
            next = null;
        }
    }
    void push(int n){
        IntStackNode node = new IntStackNode();
        node.next=top;
        top=node;
    }

    int pop(){
        if(isEmpty()){
            return -1;
        }else{
            int n = top.data;
            top = top.next;
            return n;
        }
    }

    boolean isEmpty(){
        return top==null;
    }

    public static void main(String[] args){
        DynamicIntStack myStack = new DynamicIntStack();
        myStack.push(5);
        myStack.push(10);

        IntStackNode currNode = new IntStackNode();
        while(currNode!=null){
            System.out.println(currNode.data);
            currNode = currNode.next;
        }
        System.out.println(myStack.pop());
        System.out.println(myStack.pop());
    }
}