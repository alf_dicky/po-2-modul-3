public class SeqStack{
    int top = -1;
    int memSpace[];
    int limit;

    SeqStack(){
        memSpace = new int[10];
        limit = 10;
    }

    SeqStack(int size){
        memSpace = new int[size];
        limit =size;
    }

    boolean push(int value){
        top++;
        if(top<limit){
            memSpace[top]=value;
        }else{
            top--;
            return false;
        }
        return true;
    }

    int pop(){
        int temp = -1;
        if(top>=0){
            temp = memSpace[top];
            top--;
        }else{
            return -1;
        }
        return temp;
    }
}