class TestNode{
    public static void main(String[] args){
        Node emptyList = null;

        Node head = new Node();

        head.data = 5;
        head.nextNode = new Node();
        head.nextNode.data = 10;

        head.nextNode.nextNode=null;
        
        Node currNode = head;
        while(currNode!=null){
            System.out.println(currNode.data);
            currNode=currNode.nextNode;
        }
    }
}